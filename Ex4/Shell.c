#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include "LineParser.h"
#include <string.h>
#include <errno.h>

#define MAX_LENGTH 2048
#define PATH_MAX 128

void printLocation();
void execute(cmdLine *pCmdLine);

int main()
{
	char cmd[MAX_LENGTH];
	int temp, i;
	char *str;
	cmdLine *cmdStruct;
	pid_t pid;
	while(1)
	{
		printLocation();
		fgets(cmd, MAX_LENGTH, stdin);
		if(!strcmp(cmd, "quit\n"))
		{
			return 0;
		}
		cmdStruct = parseCmdLines(cmd);
		pid = fork();
		if(cmd[0] == 'c' && cmd[1] == 'd')
		{
			for(i = 2; i < strlen(cmd); i++)	
			{
				str[i-2] = cmd[i];
			}
			errno = 0;
			chdir(str);
			if(errno)
			{
				printf("Couldn't find directory");
			}
		}
		if(pid == -1)
		{
			perror("Fork Error");
			return 0;
		}
		else if(pid == 0)
		{
			execute(cmdStruct);
		}
		else
		{
			if(cmdStruct->blocking)
			{
				waitpid(pid, &temp, 0);
			}
		}
		freeCmdLines(cmdStruct);
	}
	return 0;
}

void printLocation()
{
	char dir[PATH_MAX];
	syscall(SYS_getcwd, dir);
	printf("%s:", dir);
}

void execute(cmdLine *pCmdLine)
{
	int temp;
	char *str;
	if(pCmdLine->inputRedirect)
	{
		close(1);
		temp = open(pCmdLine->inputRedirect, O_RDONLY);
		dup(temp);
	}
	else if(pCmdLine->outputRedirect)
	{
		close(0);
		temp = open(pCmdLine->outputRedirect, O_WRONLY);
		dup(temp);
	}
	else
	{
		temp = execvp(pCmdLine->arguments[0], pCmdLine->arguments);
		if(temp == -1)
		{
			str = strerror(errno);
			perror(str);
		}
	}
}