#include "ceaser.h"

#include <stdio.h>
#include <stdlib.h>

#define MAX_LENGTH 25
#define CRT_SECURE_NO_WARNINGS

void print(char *str);

int main(int argc, char ** argv)
{
	char input[MAX_LENGTH], output[MAX_LENGTH];
	int key;
	if (argc < 2)
	{
		printf("usage: %s <mode>\n",argv[0]);
		exit(1);
	}
	int mode = (argv[1][0] == 'd') ? -1 : 1;
	printf("Enter message:\n");
	gets(input);
	printf("\nEnter Key:\n");
	scanf("%d", &key);
	shift_string(input, mode * key, output);
	puts(output);	
	return 0;
}