#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <string.h>
#include <errno.h>

int main()
{
	pid_t pid;
	pid = fork();
	if(pid == -1)
	{
		printf("Fork failed. Exiting program\n");
		return 0;
	}
	if(pid == 0)
	{
		while (1)
		{
			printf("I'm still alive\n");
		}
	}
	else
	{
		sleep(1);
		printf("Dispatching\n");
		kill(pid, SIGQUIT);
		printf("Dispatched\n");
	}
	return 0;
}