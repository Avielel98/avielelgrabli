#include "ceaser.h"
#include <stdio.h>
#include <string.h>

#define LETTERS_NUM 26

char shift_letter(char letter, int offset)
{
	return letter+offset;
}

char shift_string(char * input, int offset, char *output)
{
	int length = strlen(input);
	int i;
	char temp;
	for (i = 0; i < length; i++)
	{
		temp = shift_letter(input[i],offset);
		if(temp > 'z')
		{
			temp -= LETTERS_NUM;
		}
		output[i] = temp;
	}
	output[i] = '\0';
}