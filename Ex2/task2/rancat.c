#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_LENGTH 25
#define DECIMAL 10

int main()
{
	srand(time(NULL));
	FILE *myfile;
	char file[MAX_LENGTH], str[MAX_LENGTH] = "cat ", temp[MAX_LENGTH];
	int ch, number_of_lines = 0;
	printf("Enter file's name:");
	scanf("%s", file);
	myfile = fopen(file, "r");
	if(!myfile)
	{
		printf("Unable to open file\n");
		return 0;
	}
	do 
	{
	    ch = fgetc(myfile);
	    if(ch == '\n')
	    {
	    	number_of_lines++;
	    }

	} while (ch != EOF);

	if(ch != '\n' && number_of_lines != 0) 
	{
	    number_of_lines++;
	}

	strcat(str, file);
	strcpy(temp, " | head -n ");
	strcat(str, temp);
	snprintf(temp, MAX_LENGTH, "%d", 1 + (rand() % number_of_lines));
	strcat(str, temp);
	strcat(str, " | tail -n 1");
	system(str);
	fclose(myfile);
	return 0;
}