#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include "helper.h"

#define MAX_LENGTH 64
#define MAX_OPTIONS 4

void myExit();
void userID();
void folderName();
void getSysInfo();

int main()
{
	fun_desc functions[] = {{"exit", myExit},
							{"printUserId", userID},
							{"printCurrFolder", folderName},
							{"systemInfo", getSysInfo}};

	char str[] = "Choose on of the following options:\n1.) Close The Program\n2.) Print current user id\n3.) Show current folder name\n4.) Show Up Time & Processes count\n\0";
	char select[MAX_LENGTH];
	int selection;
	do
	{
		syscall(SYS_write, 0, str, slen(str));
		syscall(SYS_read, 0, select, sizeof(select) - 1);
		selection = select[0] - '0';
		if(selection <= MAX_OPTIONS)
		{
			functions[selection - 1].fun();
		}
		else
		{
			syscall(SYS_write, 0, "Wrong option!!\n", sizeof("Wrong option!!\n") - 1);
		}
	}while(selection != 1);
	return 0;
}

void myExit()
{
	syscall(SYS_exit);
}

void userID()
{
	int user;
	char *userStr;
	user = syscall(SYS_getuid);
	userStr = __itoa(user);
	syscall(SYS_write, 0, "Current User ID: ", sizeof("Current User ID: ") - 1);
	syscall(SYS_write, 0, userStr, slen(userStr));
	syscall(SYS_write, 0, "\n", sizeof("\n") - 1);
}

void folderName()
{
	char dir[MAX_LENGTH];
	syscall(SYS_getcwd, dir, sizeof(dir) - 1);
	syscall(SYS_write, 0, "Current Location:\n", sizeof("Current Location:\n"));
	syscall(SYS_write, 0, dir, slen(dir));
	syscall(SYS_write, 0, "\n", sizeof("\n"));
}

void getSysInfo()
{
	struct sysinfo s;
	char *temp;
 	sysinfo(&s);
 	temp = __itoa(s.uptime);
 	syscall(SYS_write, 0, "Up Time:\n", sizeof("Up Time:\n"));
 	syscall(SYS_write, 0, temp, slen(temp));
 	syscall(SYS_write, 0, "\nProcesses number:\n", sizeof("\nProcesses number:\n"));
 	temp = __itoa(s.procs);
 	syscall(SYS_write, 0, temp, slen(temp));
 	syscall(SYS_write, 0, "\n", sizeof("\n"));
}