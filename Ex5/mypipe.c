#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	pid_t pid;
	int fd[2], pipeRes = -1;
	char buffer[] = "Magshimim\0";
	pipeRes = pipe(fd);
	if(pipeRes == -1)
	{
		printf("Pipe Error!!\n");
		return 0;
	}
	pid = fork();
	if(pid < 0)
	{
		printf("Fork Error!!\n");
		return 0;
	}
	if(pid == 0)
	{
		write(fd[1], buffer, sizeof(buffer));
		strcpy(buffer, "asdasd");
		exit(2);
	}
	wait(pid);
	strcpy(buffer, "asdasd");
	read(fd[0], buffer, sizeof(buffer));
	printf("%s\n", buffer);
	return 0;
}