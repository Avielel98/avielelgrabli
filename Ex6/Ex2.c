#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <string.h>
#include <errno.h>

void quit();

int main()
{
	int i;
	signal(2, quit);
	for(i = 1; i <= 2000000000; i++)
	{
		if(i % 1000000000 == 0)
		{
			printf("I'm still alive i = %d\n", i);
		}
	}
	exit(EXIT_SUCCESS);
	return 0;
}

void quit()
{
	char str[1024];
	printf("Do you want to close the program? (y/n) \n");
	gets(str);
	if(str[0] == 'Y' || str[0] == 'y')
	{
		exit(0);
	}
}